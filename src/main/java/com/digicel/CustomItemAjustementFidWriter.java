package com.digicel;

import java.sql.Connection;
import java.sql.Statement;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.batch.item.ItemWriter;
import org.springframework.context.annotation.Scope;

import com.digicel.model.AjustementFid;
import com.digicel.model.DonneesAcquisition;

@Scope("step")
public class CustomItemAjustementFidWriter  implements ItemWriter<AjustementFid> {

	private Statement statement = null;

	private DataSource datasource;

	public  DataSource getDatasource() {
		return datasource;
	}

	public void setDatasource(DataSource datasource) {
		this.datasource = datasource;
	}
	
	@Override
	public void write(List<? extends AjustementFid> datas) throws Exception {
		 
		Connection connection = datasource.getConnection();
		 
		 for (AjustementFid data : datas)
		 {	
			 statement = connection.createStatement();
			 String query = String.format("INSERT INTO ajustement_fid_temp(anciennete,duree_reengagement,ajustement_fidelisation) VALUES ('%1$s', '%2$s', %3$s);", data.getAnciennete(), data.getDuree_engagement(), data.getValeur_calcul());
			 statement.executeUpdate(query);
		 }
	}
}
