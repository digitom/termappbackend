package com.digicel;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;

import com.digicel.model.DonneesAcquisition;
import com.digicel.model.InfosStatutTerminal;  

@Scope("step")
public class CustomItemInfosGuyReader implements ItemReader<InfosStatutTerminal>{  

private String fileName; 
private int counter ;

List<InfosStatutTerminal> myList = null;

public CustomItemInfosGuyReader()
{
	counter = 0;	
}

 @Override  
 public InfosStatutTerminal read() throws Exception, UnexpectedInputException, ParseException 
 {  
	 InfosStatutTerminal valueToReturn = null;
	 
	 if(counter >= myList.size())
	 {
		 return null;
	 }
	 
	 try
	 {
		 valueToReturn = myList.get(counter);
		 counter ++;
	 }
	 catch(Exception e) {
			e.printStackTrace();
	 }	
	 return valueToReturn;
 }
 
 
 @Value("#{jobParameters['input.infos.guy.file.name']}")
 public void setFileName(final String name) 
 {
	 this.fileName = name;	 
 
	 myList = new ArrayList<InfosStatutTerminal>();
	 BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ";";
		int iteration = 0;
		
		try {

			br = new BufferedReader(new FileReader(fileName));
			while ((line = br.readLine()) != null) {
					
				  if(iteration < 1) {
				        iteration++;  
				        continue;
				    }
				  
		        String[] lineValues = line.split(cvsSplitBy);
		        
				for(int i = 0; i <= lineValues.length-1; i++ )
				{
					InfosStatutTerminal value = new InfosStatutTerminal(lineValues[0].replace(" ", ""), lineValues[1], lineValues[2], lineValues[3], "GUY");
					myList.add(value);
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	 	  catch(Exception e) {
				e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
 	} 
 
}  
