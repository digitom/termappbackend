package com.digicel;

import java.sql.Connection;
import java.sql.Statement;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.batch.item.ItemWriter;
import org.springframework.context.annotation.Scope;

import com.digicel.model.Turboweb;

@Scope("step")
public class CustomItemTurbowebWriter implements ItemWriter<Turboweb> {
	
	private Statement statement = null;

	private DataSource datasource;

	public  DataSource getDatasource() {
		return datasource;
	}

	public void setDatasource(DataSource datasource) {
		this.datasource = datasource;
	}

	@Override
	public void write(List<? extends Turboweb> items) throws Exception {
		
		Connection connection = datasource.getConnection();
		
		for(Turboweb turboweb : items)
		{
			statement = connection.createStatement();
			String query = String.format("INSERT INTO turboweb_temp(date,nom_fichier,departement) VALUES ('%1$s', '%2$s', '%3$s');", turboweb.getDateDebut(), turboweb.getNomFichier(), turboweb.getDepartement());
			statement.executeUpdate(query);
		}
		
	}
}
