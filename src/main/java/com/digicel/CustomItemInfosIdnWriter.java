package com.digicel;

import java.sql.Connection;
import java.sql.Statement;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.batch.item.ItemWriter;
import org.springframework.context.annotation.Scope;

import com.digicel.model.DonneesAcquisition;
import com.digicel.model.InfosStatutTerminal;
import com.digicel.model.MappingReferenceGencode;  
  
@Scope("step")
public class CustomItemInfosIdnWriter implements ItemWriter<InfosStatutTerminal> {  
  
private Statement statement = null;

private DataSource datasource;

public  DataSource getDatasource() {
	return datasource;
}

public void setDatasource(DataSource datasource) {
	this.datasource = datasource;
}

@Override
public void write(List<? extends InfosStatutTerminal> datas) throws Exception {
	
	 Connection connection = datasource.getConnection();
		 
	 try 
	 {
		 for (InfosStatutTerminal data : datas)
		 {	
			 statement = connection.createStatement();
			 String queryIdn = String.format("update statut_terminal_temp set statut = '%1$s' WHERE gencode = '%2$s' AND departement = '%3$s'",data.getStatut(),data.getGencod(),data.getDepartement());
			 statement.executeUpdate(queryIdn);
		 }
	 }
	 catch (Exception e) 
	 {
		 throw new RuntimeException(e.getMessage());
	 }
	 
	}
}  
