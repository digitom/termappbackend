package com.digicel.services;

import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

//import org.jboss.seam.annotations.Name;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;

@Stateless
//@Name("SendMail")
public class SendMailService {

	private JavaMailSender mailSender;
	private String senderAddress;
	private String recipient;
	private String subject;
	private String body;

	// set the fields
	public void setFields(JavaMailSender mailSender, String senderAddress, String recipient, String subject,
			String body) {

		this.mailSender = mailSender;
		this.senderAddress = senderAddress;
		this.recipient = recipient;
		this.subject = subject;
		this.body = body;
	}

	public void sendMail() {

		MimeMessagePreparator preparator = new MimeMessagePreparator() {
			public void prepare(MimeMessage mimeMessage) throws Exception {
				mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
				mimeMessage.setFrom(new InternetAddress(senderAddress));
				mimeMessage.setSubject(subject);
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
				helper.setText(body);
			}
		};

		this.mailSender.send(preparator);
	}
}
