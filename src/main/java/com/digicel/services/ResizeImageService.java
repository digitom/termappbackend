package com.digicel.services;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.apache.commons.io.FilenameUtils;
import org.imgscalr.Scalr;


public class ResizeImageService {

	private static final int IMG_WIDTH = 200;
	private static final int IMG_HEIGHT = 100;
	
	private final static int MAX_WIDTH_OR_HEIGHT = 400;
	private final static int PADDING = 10;

	
	public static BufferedImage fillTransparentPixels( BufferedImage image, Color fillColor) {
			
			int w = image.getWidth();
		    int h = image.getHeight();
			BufferedImage image2 = new BufferedImage(w, h, 
			BufferedImage.TYPE_INT_RGB);
			Graphics2D g = image2.createGraphics();
			g.setColor(fillColor);
			g.fillRect(0,0,w,h);
			g.drawRenderedImage(image, null);
			g.dispose();
			return image2;
		}
	
	
    public void ResizeImage(File file,String destinationPath)
    {    	
    try 
		{	
    		System.out.println("image en cours ... ");
			System.out.println(file.getAbsolutePath());
						
			String destination = destinationPath + file.getName();
			String fileNameWithoutExtension = FilenameUtils.removeExtension(destination);
			process(file,new File(fileNameWithoutExtension+".jpg"));
			
			System.out.println("image copiee ... ");
			System.out.println(fileNameWithoutExtension);
		}
	    catch (Exception e) {
	      	throw new RuntimeException(e.getMessage());
		}
    }
    
	public void process(final File inputFile, final File outputFile) throws Exception
	{
		BufferedImage input = null;
		BufferedImage resized = null;
		BufferedImage trimmed = null;
		BufferedImage padded = null;
		BufferedImage output = null;

		try
		{
			input = ImageIO.read(inputFile);
			trimmed = trim(input);
			resized = resize(trimmed);
			padded = pad(resized);
			output = background(padded);

			ImageIO.write(output, "jpg", outputFile);
		}
		catch (Exception e)
		{
			throw e;
		}
		finally
		{
			if (input != null)
				input.flush();

			if (resized != null)
				resized.flush();

			if (trimmed != null)
				trimmed.flush();

			if (padded != null)
				padded.flush();

			if (output != null)
				output.flush();
		}
	}

    
    public BufferedImage resize(final BufferedImage input)
	{
		Scalr.Method scalingMethod = Scalr.Method.ULTRA_QUALITY;
		Scalr.Mode resizeMode = Scalr.Mode.AUTOMATIC;

		int w = MAX_WIDTH_OR_HEIGHT - PADDING;

		return Scalr.resize(input, scalingMethod, resizeMode, w, w);
	}

	public BufferedImage pad(final BufferedImage input)
	{
		return Scalr.pad(input, PADDING, Color.WHITE);
	}

	public BufferedImage trim(final BufferedImage input)
	{
		int x1 = Integer.MAX_VALUE, y1 = Integer.MAX_VALUE, x2 = 0, y2 = 0;

		final int topLeftPixel = input.getRGB(0, 0);

		for (int x = 0; x < input.getWidth(); x++)
		{
			for (int y = 0; y < input.getHeight(); y++)
			{
				int argb = input.getRGB(x, y);
				if (argb != topLeftPixel)
				{
					x1 = Math.min(x1, x);
					y1 = Math.min(y1, y);
					x2 = Math.max(x2, x);
					y2 = Math.max(y2, y);
				}
			}
		}

		int w = x2 - x1;
		int h = y2 - y1;

		return Scalr.crop(input, x1, y1, w, h);
	}

	public BufferedImage background(final BufferedImage image)
	{
		BufferedImage output = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
		output.createGraphics().drawImage(image, 0, 0, Color.WHITE, null);

		return output;
	}
	
}
