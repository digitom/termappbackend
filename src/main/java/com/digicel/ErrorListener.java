package com.digicel;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;

public class ErrorListener implements StepExecutionListener 
{
	public ExitStatus afterStep(StepExecution stepExecution) {
		String exitCode = stepExecution.getExitStatus().getExitCode();
		if (exitCode.equals(ExitStatus.FAILED.getExitCode())) {
			StringBuilder messages = new StringBuilder();
			for (Throwable error : stepExecution.getFailureExceptions()) {
				messages.append(error.getMessage());
			}
			stepExecution.getJobExecution().getExecutionContext().put("error", messages.toString());
			return stepExecution.getExitStatus();
		}
		return ExitStatus.COMPLETED;
	}

	public void beforeStep(StepExecution stepExecution) {
		// Nothing to do here.
	}

}