package com.digicel.model;

public class MappingOffreColonne {
	
	private String Code_offre;
	private String Nom_regroupement;
	private int Colonne_turbo_web;
	
	public String getCode_offre() {
		return Code_offre;
	}
	
	public void setCode_offre(String code_offre) {
		Code_offre = code_offre;
	}

	public String getNom_regroupement() {
		return Nom_regroupement;
	}

	public void setNom_regroupement(String nom_regroupement) {
		Nom_regroupement = nom_regroupement;
	}

	public int getColonne_turbo_web() {
		return Colonne_turbo_web;
	}

	public void setColonne_turbo_web(int colonne_turbo_web) {
		Colonne_turbo_web = colonne_turbo_web;
	}
}
