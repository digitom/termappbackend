package com.digicel.model;

public class Turboweb {

	private String DateDebut;
	private String NomFichier;
	private String Departement;
	
	public Turboweb(String dateDebut, String nomFichier, String departement) {
		DateDebut = dateDebut;
		NomFichier = nomFichier;
		Departement = departement;
	}
	
	public String getDateDebut() {
		return DateDebut;
	}
	public void setDateDebut(String dateDebut) {
		DateDebut = dateDebut;
	}
	public String getNomFichier() {
		return NomFichier;
	}
	public void setNomFichier(String nomFichier) {
		NomFichier = nomFichier;
	}
	public String getDepartement() {
		return Departement;
	}
	public void setDepartement(String departement) {
		Departement = departement;
	}
}
