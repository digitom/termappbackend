package com.digicel.model;

public class DonneesAcquisition {
	
	private String Gencode;
	private int PrixCessionDistributeur;
	private String StatutTerminal;
	private int ValeurCalcul;
	private int ColonneTurboweb;
	private String TypeInformation;
	private String DateDebut;
	private String Departement;
	
	public DonneesAcquisition(String gencode, int prixCessionDistributeur, String statutTerminal, int valeurCalcul, int colonneTurboweb, String typeInformation, String dateDebut, String departement) 
	{
		Gencode = gencode;
		PrixCessionDistributeur = prixCessionDistributeur;
		StatutTerminal = statutTerminal;
		ValeurCalcul = valeurCalcul;
		ColonneTurboweb = colonneTurboweb;
		TypeInformation = typeInformation;
		DateDebut = dateDebut;
		Departement = departement;
	}

	public String getGencode() {
		return Gencode;
	}
	
	public void setGencode(String gencode) {
		Gencode = gencode;
	}
	
	public String getStatutTerminal() {
		return StatutTerminal;
	}
	
	public void setStatutTerminal(String statutTerminal) {
		StatutTerminal = statutTerminal;
	}
	
	public int getValeurCalcul() {
		return ValeurCalcul;
	}
	
	public void setValeurCalcul(int valeurCalcul) {
		ValeurCalcul = valeurCalcul;
	}
	
	public int getColonneTurboweb() {
		return ColonneTurboweb;
	}
	
	public void setColonneTurboweb(int colonneTurboweb) {
		ColonneTurboweb = colonneTurboweb;
	}
	
	public String getTypeInformation() {
		return TypeInformation;
	}
	
	public void setTypeInformation(String typeInformation) {
		TypeInformation = typeInformation;
	}

	public int getPrixCessionDistributeur() {
		return PrixCessionDistributeur;
	}

	public void setPrixCessionDistributeur(int prixCessionDistributeur) {
		PrixCessionDistributeur = prixCessionDistributeur;
	}

	public String getDateDebut() {
		return DateDebut;
	}

	public void setDateDebut(String dateDebut) {
		DateDebut = dateDebut;
	}

	public String getDepartement() {
		return Departement;
	}

	public void setDepartement(String departement) {
		Departement = departement;
	}
	
}
