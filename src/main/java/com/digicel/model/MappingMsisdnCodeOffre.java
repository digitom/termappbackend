package com.digicel.model;

public class MappingMsisdnCodeOffre {
	
	private String Code_offre;
	private String Msisdn;
	private String Nom_offre;
	private String Departement;
	
	public String getCode_offre() {
		return Code_offre;
	}
	
	public void setCode_offre(String code_offre) {
		Code_offre = code_offre;
	}
	
	public String getMsisdn() {
		return Msisdn;
	}
	
	public void setMsisdn(String msisdn) {
		Msisdn = msisdn;
	}
	
	public String getNom_offre() {
		return Nom_offre;
	}
	
	public void setNom_offre(String nom_offre) {
		Nom_offre = nom_offre;
	}

	public String getDepartement() {
		return Departement;
	}

	public void setDepartement(String departement) {
		Departement = departement;
	}

}
