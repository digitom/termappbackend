package com.digicel.model;

public class AjustementFid {
	
	private String Anciennete;
	private String Duree_engagement;
	private int Valeur_calcul;
	
	public AjustementFid(String anciennete, String duree_engagement, int valeur_calcul)
	{
		this.Anciennete = anciennete;
		this.Duree_engagement = duree_engagement;
		this.Valeur_calcul = valeur_calcul;
	}
	
	public String getAnciennete() {
		return Anciennete;
	}
	public void setAnciennete(String anciennete) {
		Anciennete = anciennete;
	}
	
	public String getDuree_engagement() {
		return Duree_engagement;
	}
	public void setDuree_engagement(String duree_engagement) {
		Duree_engagement = duree_engagement;
	}
	
	public int getValeur_calcul() {
		return Valeur_calcul;
	}
	public void setValeur_calcul(int valeur_calcul) {
		Valeur_calcul = valeur_calcul;
	}
}
