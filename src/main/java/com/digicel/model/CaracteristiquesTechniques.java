package com.digicel.model;

public class CaracteristiquesTechniques {
	
	private String Modele_mobi;
	private String Marque;
	private String Modele;
	private String Type_sim;
	private String Design;
	private String xG;
	private String Smartphone;
	private String Das;
	private String Dual_sim;
	private String Poids;
	private String Memoire; 
	private String Dimension;
	private String Band;
	private String Bande;
	private String Ecran_couleur;
	private String Taille_ecran;
	private String Processeur;
	private String Os;
	private String Video;
	private String Bluetooth;
	private String Radio;
	private String Photo;
	private String Mp3;
	private String Wifi;
	private String Autonomie;
	private String Description;
	private String Trimestre;
	
	
	public String getModele_mobi() {
		return Modele_mobi;
	}
	public void setModele_mobi(String modele_mobi) {
		this.Modele_mobi = modele_mobi;
	}
	public String getMarque() {
		return Marque;
	}
	public void setMarque(String marque) {
		Marque = marque;
	}
	public String getModele() {
		return Modele;
	}
	public void setModele(String modele) {
		Modele = modele;
	}
	public String getType_sim() {
		return Type_sim;
	}
	public void setType_sim(String type_sim) {
		Type_sim = type_sim;
	}
	public String getDesign() {
		return Design;
	}
	public void setDesign(String design) {
		Design = design;
	}
	public String getxG() {
		return xG;
	}
	public void setxG(String xg) {
		xG = xg;
	}
	public String getSmartphone() {
		return Smartphone;
	}
	public void setSmartphone(String smartphone) {
		Smartphone = smartphone;
	}
	public String getDas() {
		return Das;
	}
	public void setDas(String das) {
		Das = das;
	}
	public String getDual_sim() {
		return Dual_sim;
	}
	public void setDual_sim(String dual_sim) {
		Dual_sim = dual_sim;
	}
	public String getPoids() {
		return Poids;
	}
	public void setPoids(String poids) {
		Poids = poids;
	}
	public String getMemoire() {
		return Memoire;
	}
	public void setMemoire(String memoire) {
		Memoire = memoire;
	}
	public String getDimension() {
		return Dimension;
	}
	public void setDimension(String dimension) {
		Dimension = dimension;
	}
	public String getBand() {
		return Band;
	}
	public void setBand(String band) {
		Band = band;
	}
	public String getBande() {
		return Bande;
	}
	public void setBande(String bande) {
		Bande = bande;
	}
	public String getEcran_couleur() {
		return Ecran_couleur;
	}
	public void setEcran_couleur(String ecran_couleur) {
		Ecran_couleur = ecran_couleur;
	}
	public String getTaille_ecran() {
		return Taille_ecran;
	}
	public void setTaille_ecran(String taille_ecran) {
		Taille_ecran = taille_ecran;
	}
	public String getProcesseur() {
		return Processeur;
	}
	public void setProcesseur(String processeur) {
		Processeur = processeur;
	}
	public String getOs() {
		return Os;
	}
	public void setOs(String os) {
		Os = os;
	}
	public String getVideo() {
		return Video;
	}
	public void setVideo(String video) {
		Video = video;
	}
	public String getBluetooth() {
		return Bluetooth;
	}
	public void setBluetooth(String bluetooth) {
		Bluetooth = bluetooth;
	}
	public String getRadio() {
		return Radio;
	}
	public void setRadio(String radio) {
		Radio = radio;
	}
	public String getPhoto() {
		return Photo;
	}
	public void setPhoto(String photo) {
		Photo = photo;
	}
	public String getMp3() {
		return Mp3;
	}
	public void setMp3(String mp3) {
		Mp3 = mp3;
	}
	public String getWifi() {
		return Wifi;
	}
	public void setWifi(String wifi) {
		Wifi = wifi;
	}
	public String getAutonomie() {
		return Autonomie;
	}
	public void setAutonomie(String autonomie) {
		Autonomie = autonomie;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	private String getTrimestre() {
		return Trimestre;
	}
	private void setTrimestre(String trimestre) {
		Trimestre = trimestre;
	}

}
