package com.digicel.model;

public class MappingReferenceGencode {
	
	private String Modele_generique;
	private String Modele_mobi;
	private String Gencode;
	
	
	public String getGencode() {
		return Gencode;
	}
	
	public void setGencode(String gencode) {
		Gencode = gencode;
	}

	public String getModele_generique() {
		return Modele_generique;
	}

	public void setModele_generique(String modele_generique) {
		Modele_generique = modele_generique;
	}

	public String getModele_mobi() {
		return Modele_mobi;
	}

	public void setModele_mobi(String modele_mobi) {
		Modele_mobi = modele_mobi;
	}	
}
