package com.digicel.model;

public class InfosStatutTerminal {
	
	private String Gencod;
	private String Marque;
	private String Modele;
	private String Statut;
	private String Departement;
	
	public InfosStatutTerminal(String gencod, String marque, String modele, String statut, String departement) {
		super();
		Gencod = gencod;
		Marque = marque;
		Modele = modele;
		Statut = statut;
		Departement = departement;
	}
	
	public String getGencod() {
		return Gencod;
	}
	public void setGencod(String gencod) {
		Gencod = gencod;
	}
	public String getMarque() {
		return Marque;
	}
	public void setMarque(String marque) {
		Marque = marque;
	}
	public String getModele() {
		return Modele;
	}
	public void setModele(String modele) {
		Modele = modele;
	}
	public String getStatut() {
		return Statut;
	}
	public void setStatut(String statut) {
		Statut = statut;
	}
	public String getDepartement() {
		return Departement;
	}
	public void setDepartement(String departement) {
		Departement = departement;
	}
	
}
