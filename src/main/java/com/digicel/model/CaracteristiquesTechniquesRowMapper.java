package com.digicel.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CaracteristiquesTechniquesRowMapper implements RowMapper<CaracteristiquesTechniques> {

	@Override
	public CaracteristiquesTechniques mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		CaracteristiquesTechniques caracteristiquesTechniques = new CaracteristiquesTechniques();
		
		caracteristiquesTechniques.setModele_mobi(rs.getString("modele_mobi"));
		caracteristiquesTechniques.setMarque(rs.getString("marque"));
		caracteristiquesTechniques.setModele(rs.getString("modele"));
		caracteristiquesTechniques.setType_sim(rs.getString("type_sim"));
		caracteristiquesTechniques.setDesign(rs.getString("design"));
		caracteristiquesTechniques.setxG(rs.getString("xG"));
		caracteristiquesTechniques.setSmartphone(rs.getString("smartphone"));
		caracteristiquesTechniques.setDas(rs.getString("das"));
		caracteristiquesTechniques.setDual_sim(rs.getString("dual_sim"));
		caracteristiquesTechniques.setPoids(rs.getString("poids"));
		caracteristiquesTechniques.setMemoire(rs.getString("memoire"));
		caracteristiquesTechniques.setDimension(rs.getString("dimension"));
		caracteristiquesTechniques.setBand(rs.getString("band"));
		caracteristiquesTechniques.setBande(rs.getString("bande"));
		caracteristiquesTechniques.setEcran_couleur(rs.getString("ecran_couleur"));
		caracteristiquesTechniques.setTaille_ecran(rs.getString("taille_ecran"));
		caracteristiquesTechniques.setProcesseur(rs.getString("processeur"));
		caracteristiquesTechniques.setOs(rs.getString("os"));
		caracteristiquesTechniques.setVideo(rs.getString("video"));
		caracteristiquesTechniques.setBluetooth(rs.getString("bluetooth"));
		caracteristiquesTechniques.setRadio(rs.getString("radio"));
		caracteristiquesTechniques.setPhoto(rs.getString("photo"));
		caracteristiquesTechniques.setMp3(rs.getString("mp3"));
		caracteristiquesTechniques.setWifi(rs.getString("wifi"));
		caracteristiquesTechniques.setAutonomie(rs.getString("autonomie"));
		caracteristiquesTechniques.setDescription(rs.getString("description"));
		
		return caracteristiquesTechniques;
	}
}
