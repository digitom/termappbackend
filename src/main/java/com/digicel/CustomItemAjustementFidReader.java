package com.digicel;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;

import com.digicel.model.AjustementFid;

@Scope("step")
public class CustomItemAjustementFidReader implements ItemReader<AjustementFid>  {

	private String fileName; 
	private int counter ;

	List<AjustementFid> myList = null;

	@Override
	public AjustementFid read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		
		AjustementFid valueToReturn = null;
		 
		 if(counter >= myList.size())
		 {
			 return null;
		 }
		 
		 try
		 {
			 valueToReturn = myList.get(counter);
			 counter ++;
		 }
		 catch(Exception e) {
				e.printStackTrace();
		 }
		 
		 return valueToReturn;
	}
	
	
	@Value("#{jobParameters['input.ajustementfid.file.name']}")
	 public void setFileName(final String name) 
	 {
		 this.fileName = name;	 
	 
		 myList = new ArrayList<AjustementFid>();
		 BufferedReader br = null;
			
		 String line = "";
			String cvsSplitBy = ";";
			int iteration = 0;
			
			try {

				br = new BufferedReader(new FileReader(fileName));
				while ((line = br.readLine()) != null) {
					
					  if(iteration < 1) {
					        iteration++;  
					        continue;
					    }
					
			        String[] lineValues = line.split(cvsSplitBy);
			        
			        //TODO : a refaire si on a le temps
					AjustementFid value12 = new AjustementFid(lineValues[0].replace(" ", ""),"12 MOIS",Integer.parseInt(lineValues[1].replace(" ", "")));
					AjustementFid value24 = new AjustementFid(lineValues[0].replace(" ", ""),"24 MOIS",Integer.parseInt(lineValues[2].replace(" ", "")));
					
					myList.add(value12);
					myList.add(value24);
				}

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}catch(Exception e) {
					e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
	 	}	
	 
}
