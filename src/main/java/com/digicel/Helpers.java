package com.digicel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

public class Helpers {
	 
	public static DataStructure GenerateMsisdnOffreFileJobParameters(File folder)
	{
		DataStructure result = null;
		
		File[] listOfFiles = folder.listFiles();		
		
		for (File file : listOfFiles) {
		    if (file.isFile()) {
		        if(file.getName().contains("msisdnOffers"))
		        {
		        	result = new DataStructure();
		        	result.path = file.getPath(); 
		        	result.columnValue = file.getName().subSequence(0, 8).toString();  
		        }
		    }
		}
		return result;
	}
	
	public static DataStructure GenerateCarateristiquesTechniquesJobParameters(File folder)
	{
		File[] listOfFiles = folder.listFiles();		
		DataStructure result = null;
		
		for (File file : listOfFiles) {
		    if (file.isFile()) {
		        if(file.getName().contains("Q"))
		        {
		        	result = new DataStructure();
		        	result.path = file.getPath(); 
		        	result.columnValue = file.getName().replace(".csv",""); 
		        }
		    }
		}
		return result;
	}
	
	
	public static DataStructure GenerateAjustementFidJobParameters(File folder)
	{
		DataStructure result = null;
		
		File[] listOfFiles = folder.listFiles();		
		
		for (File file : listOfFiles) {
		    if (file.isFile()) {
		        if(file.getName().contains("ajustementFid"))
		        {
		        	result = new DataStructure();
		        	result.path = file.getPath();
		        }
		    }
		}
		return result;
	}
	
	public static DataStructure GenerateOffreColonneJobParameters(File folder)
	{
		DataStructure result = null;
		
		File[] listOfFiles = folder.listFiles();		
		
		for (File file : listOfFiles) {
		    if (file.isFile()) {
		        if(file.getName().contains("offreColonne"))
		        {
		        	result = new DataStructure();
		        	result.path = file.getPath(); 
		        	result.columnValue = file.getName().subSequence(0, 8).toString();  
		        }
		    }
		}
		return result;
	}
	
	public static DataStructure GenerateTurbowebJobParameters(File folder,String departement)
	{
		File[] listOfFiles = folder.listFiles();		
		DataStructure result = null;
		
		for (File file : listOfFiles) {
		    if (file.isFile()) {
		        if(file.getName().contains(departement+"-turboweb.xls"))
		        {
		        	result = new DataStructure();
		        	result.path = file.getPath(); 
		        	result.columnValue = file.getName().subSequence(0, 8).toString();  
		        	file.getName().subSequence(9, 12).toString();
		        }
		    }
		}
		return result;
	}
	
	public static DataStructure GenerateTurbowebInfosJobParameters(File folder,String departement)
	{
		DataStructure result = null;
		
		File[] listOfFiles = folder.listFiles();		
		
		for (File file : listOfFiles) {
		    if (file.isFile()) {
		        if(file.getName().contains(departement+"-turboweb-infos"))
		        {
		        	result = new DataStructure();
		        	result.path = file.getPath(); 
		        	result.columnValue = file.getName().subSequence(0, 8).toString();  
		        	file.getName().subSequence(9, 12).toString();
		        }
		    }
		}
		return result;
	}
	
	public static DataStructure GenerateTurbowebRenavJobParameters(File folder,String departement)
	{
		DataStructure result = null;
		
		File[] listOfFiles = folder.listFiles();		
		
		for (File file : listOfFiles) {
		    if (file.isFile()) {
		        if(file.getName().contains(departement+"-turboweb-renAV"))
		        {
		        	result = new DataStructure();
		        	result.path = file.getPath(); 
		        	result.columnValue = file.getName().subSequence(0, 8).toString();  
		        	file.getName().subSequence(9, 12).toString();
		        }
		    }
		}
		return result;
	}
	
	public static DataStructure GenerateTurbowebContporJobParameters(File folder,String departement)
	{
		DataStructure result = null;
		
		File[] listOfFiles = folder.listFiles();		
		
		for (File file : listOfFiles) {
		    if (file.isFile()) {
		        if(file.getName().contains(departement+"-turboweb-contPOR"))
		        {
		        	result = new DataStructure();
		        	result.path = file.getPath(); 
		        	result.columnValue = file.getName().subSequence(0, 8).toString();  
		        	file.getName().subSequence(9, 12).toString();
		        }
		    }
		}
		return result;
	}
	
	public static DataStructure GenerateTurbowebContavJobParameters(File folder,String departement)
	{
		DataStructure result = null;
		
		File[] listOfFiles = folder.listFiles();		
		
		for (File file : listOfFiles) {
		    if (file.isFile()) {
		        if(file.getName().contains(departement+"-turboweb-contAV"))
		        {
		        	result = new DataStructure();
		        	result.path = file.getPath(); 
		        	result.columnValue = file.getName().subSequence(0, 8).toString(); 
		        	file.getName().subSequence(9, 12).toString();
		        }
		    }
		}
		return result;
	}
	
	public static DataStructure GenerateReferenceGencodeJobParameters(File folder)
	{
		DataStructure result = null;
		
		File[] listOfFiles = folder.listFiles();		
		
		for (File file : listOfFiles) {
		    if (file.isFile()) {
		        if(file.getName().contains("gencodModele"))
		        {
		        	result = new DataStructure();
		        	result.path = file.getPath(); 
		        	result.columnValue = file.getName().subSequence(0, 8).toString();  
		        }
		    }
		}
		return result;
	}
	
	
	public static void MoveFile(String sourcePath)
	{
		File aFile = new File(sourcePath);
		String path = FilenameUtils.getPath(sourcePath);
		aFile.renameTo(new File("/"+path + "processed/" + aFile.getName()));
	}
	
	public static boolean CheckDataStructurePath(List<DataStructure> dataStructures)
	{
		for (DataStructure dataStructure : dataStructures) {
			if (dataStructure == null) {
				return false;
			}
		}
		return true;
	}
	
	public static void DeleteFolder(String folderPath) throws IOException
	{	
		List<FolderDataStructure> dataStructures = GetFolders(folderPath);
		
		for(FolderDataStructure dataStructure: dataStructures)
		{
			FileUtils.deleteDirectory(new File(dataStructure.path));
		}
	} 
	
	public static boolean CreateFolder(String folderPath)
	{
		return (new File(folderPath)).mkdirs();
	}
	
	public  static List<FolderDataStructure> GetFiles(String folder)
	{
		List<FolderDataStructure> fileList = new ArrayList<FolderDataStructure>();
		File folderHandle = new File(folder);
		String[] names = folderHandle.list();
		
		for(String name : names)
		{
			File folderFile = new File(folder + name);	
		    if (folderFile.isFile())
		    {
		    	fileList.add(new FolderDataStructure(folder + name,folder));
		    }
		}	
		return fileList;
	}
	
	public static List<FolderDataStructure> GetFolders(String folder)
	{
		List<FolderDataStructure> folderList = new ArrayList<FolderDataStructure>();
		File folderHandle = new File(folder);
		String[] names = folderHandle.list();
		
		for(String name : names)
		{
			File folderFile = new File(folder + name);	
		    
			if(folderFile.isDirectory())
		    {
		    	folderList.add(new FolderDataStructure(folder + name + "/", name));
		    }
		}	
		return folderList;
	}

}
