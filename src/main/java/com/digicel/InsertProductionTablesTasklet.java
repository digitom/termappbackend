package com.digicel;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.io.FilenameUtils;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

public class InsertProductionTablesTasklet implements Tasklet {
	
	private String imagesTraiteesFolder = null;
	private static final String delete_sql_ajustement_fid = "delete from ajustement_fid;";
	private static final String delete_sql_caracteristiques_techniques = "delete from caracteristiques_techniques;";
	private static final String delete_sql_mapping_msisdn_code_offre = "delete from mapping_msisdn_code_offre;";
	private static final String delete_sql_mapping_offre_segment = "delete from mapping_offre_segment;";
	private static final String delete_sql_mapping_reference_gencode = "delete from mapping_reference_gencode;";
	private static final String delete_sql_informations_turboweb = "delete from informations_turboweb;";
	private static final String delete_sql_images_terminaux = "delete from images_terminaux;";
	private static final String delete_sql_statut_terminal = "delete from statut_terminal;";
	private static final String delete_sql_turboweb = "delete from turboweb;";
	
	private static final String INSERT_AJUSTEMENT_FID = "insert into ajustement_fid(anciennete,duree_reengagement,ajustement_fidelisation) select anciennete,duree_reengagement,ajustement_fidelisation from ajustement_fid_temp";
	private static final String INSERT_CARACTERISTIQUES_TECHNIQUES = "insert into caracteristiques_techniques(modele_mobi,marque,modele,type_sim,design,xG,smartphone,das,dual_sim,poids,memoire,dimension,band,bande,ecran_couleur,taille_ecran,processeur,os,video,bluetooth,radio,photo,mp3,wifi,autonomie,description,trimestre ) SELECT  modele_mobi,marque,modele,type_sim,design,xG,smartphone,das,dual_sim,poids,memoire,dimension,band,bande,ecran_couleur,taille_ecran,processeur,os,video,bluetooth,radio,photo,mp3,wifi,autonomie,description,trimestre FROM caracteristiques_techniques_temp;";
	private static final String INSERT_MAPPING_MSISDN_CODE_OFFRE = "insert into mapping_msisdn_code_offre(msisdn,code_offre,nom_offre,date_debut,departement) select msisdn,code_offre,nom_offre,date_debut,departement from mapping_msisdn_code_offre_temp";
	private static final String INSERT_MAPPING_REFERENCE_GENCODE = "insert into mapping_reference_gencode(modele_generique, modele_mobi, gencode, date_debut) select modele_generique, modele_mobi, gencode, date_debut from mapping_reference_gencode_temp";
	private static final String INSERT_TURBOWEB_CONTAV = "insert into informations_turboweb(gencode,prix_cession_distributeur,colonne_turboweb,type_information,date_debut,valeur_calcul,departement) select gencode,prix_cession_distributeur,colonne_turboweb,type_information,date_debut,valeur_calcul,departement from informations_turboweb_temp where type_information = 'contav'; "; 
	private static final String INSERT_TURBOWEB_CONTPOR = "insert into informations_turboweb(gencode,prix_cession_distributeur,colonne_turboweb,type_information,date_debut,valeur_calcul,departement) select gencode,prix_cession_distributeur,colonne_turboweb,type_information,date_debut,valeur_calcul,departement from informations_turboweb_temp where type_information = 'contpor'; "; 
	private static final String INSERT_TURBOWEB_RENAV = "insert into informations_turboweb(gencode,prix_cession_distributeur,colonne_turboweb,type_information,date_debut,valeur_calcul,departement) select gencode,prix_cession_distributeur,colonne_turboweb,type_information,date_debut,valeur_calcul,departement from informations_turboweb_temp where type_information = 'renav'; "; 
	private static final String INSERT_MAPPING_OFFRE_SEGMENT = "insert into mapping_offre_segment(code_offre,nom_regroupement,colonne_turboweb,date_debut) select code_offre,nom_regroupement,colonne_turboweb,date_debut from mapping_offre_segment_temp";
	private static final String INSERT_STATUT_TERMINAL = "insert into statut_terminal(gencode,statut,departement) select gencode,statut,departement from statut_terminal_temp";
	private static final String INSERT_TURBOWEB = "insert into turboweb (date,nom_fichier,departement) select date,nom_fichier,departement from turboweb_temp";
 	
	Statement statement = null;
	
	private DataSource datasource;
	
	public  DataSource getDatasource() {
		return datasource;
	}

	public void setDatasource(DataSource datasource) {
		this.datasource = datasource;
	}
    
	private static Properties LoadProperties()
	{	
		Properties prop = new Properties();
		InputStream input = null;
		
		try {
			
			input = App.class.getResourceAsStream("/spring/batch/config/config.properties");
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return prop;
	}
	
	   private void InsertDatasInDatabase() throws SQLException
	    {	    	 
	    	List<FolderDataStructure> brandsFolderList = Helpers.GetFolders(imagesTraiteesFolder);
			
			for(FolderDataStructure brandFolder : brandsFolderList)
			{					
				List<FolderDataStructure> referenceFolders = Helpers.GetFolders(brandFolder.path);
				
				for(FolderDataStructure referenceFolder : referenceFolders)
				{
					List<FolderDataStructure> fileList = Helpers.GetFiles(referenceFolder.path);	
					
					for(FolderDataStructure filePath : fileList)
					{						
						 try {
							    String modele_mobi = referenceFolder.folderName.subSequence(0, 9).toString();
							    String value = "/"+brandFolder.folderName + "/" +referenceFolder.folderName.subSequence(0, 9).toString()+"/" + FilenameUtils.getName(filePath.path);
							    String query = String.format("INSERT INTO images_terminaux(modele_mobi,path) VALUES ('%1$s', '%2$s');",modele_mobi, value); 
						      	statement.executeUpdate(query); 
					 		} 
							catch (Exception e) 
							{
								 throw new RuntimeException(e.getMessage()); 
							}
						 
					}
				}
			} 
	    }
	
	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		
		Properties properties = LoadProperties();
    	
		properties.getProperty("imagesFolder");
		imagesTraiteesFolder = properties.getProperty("imagesTraiteesFolder");
		properties.getProperty("saveFolder");
		
		Connection connection = datasource.getConnection();
		 
		 if (connection.getAutoCommit()) 
		 {
		 	connection.setAutoCommit(false);
		 }
		  
		 try {
		  
			  statement = connection.createStatement();
			  
			  statement.executeUpdate(delete_sql_ajustement_fid);
			  statement.executeUpdate(delete_sql_mapping_reference_gencode);
			  statement.executeUpdate(delete_sql_statut_terminal);
			  
			  statement.executeUpdate(delete_sql_images_terminaux);
			  statement.executeUpdate(delete_sql_caracteristiques_techniques);
			  
			  statement.executeUpdate(delete_sql_mapping_msisdn_code_offre);
			  statement.executeUpdate(delete_sql_mapping_offre_segment);
			  statement.executeUpdate(delete_sql_informations_turboweb);
			  statement.executeUpdate(delete_sql_turboweb);
			  
			  statement.executeUpdate(INSERT_AJUSTEMENT_FID);
			  statement.executeUpdate(INSERT_CARACTERISTIQUES_TECHNIQUES);
			  statement.executeUpdate(INSERT_MAPPING_MSISDN_CODE_OFFRE);
			  statement.executeUpdate(INSERT_MAPPING_REFERENCE_GENCODE);
			  statement.executeUpdate(INSERT_STATUT_TERMINAL);
			  
			  statement.executeUpdate(INSERT_TURBOWEB_CONTAV);
			  statement.executeUpdate(INSERT_TURBOWEB_CONTPOR);
			  statement.executeUpdate(INSERT_TURBOWEB_RENAV);
			  
			  statement.executeUpdate(INSERT_TURBOWEB);
			  
			  InsertDatasInDatabase();
			  
			  statement.executeUpdate(INSERT_MAPPING_OFFRE_SEGMENT);
			  
			  connection.commit();  
		} 
		catch (Exception e) 
		{
			 connection.rollback();
			 throw new RuntimeException(e.getMessage()); 
		}
		 
		return RepeatStatus.FINISHED;
	}
}
