package com.digicel;

import java.sql.Connection;
import java.sql.Statement;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.batch.item.ItemWriter;
import org.springframework.context.annotation.Scope;

import com.digicel.model.DonneesAcquisition;

@Scope("step")
public class CustomItemRenAvWriter implements ItemWriter<DonneesAcquisition> {

	private Statement statement = null;

	private DataSource datasource;

	public  DataSource getDatasource() {
		return datasource;
	}

	public void setDatasource(DataSource datasource) {
		this.datasource = datasource;
	}
	
	@Override
	public void write(List<? extends DonneesAcquisition> datas) throws Exception {
		
		 Connection connection = datasource.getConnection();
		 
		 for (DonneesAcquisition data : datas)
		 {	
			 statement = connection.createStatement();
			 String query = String.format("INSERT INTO informations_turboweb_temp(gencode,statut_terminal,prix_cession_distributeur,colonne_turboweb,type_information,date_debut,valeur_calcul,departement) VALUES (%1$s, '%2$s', %3$s, %4$s, '%5$s', %6$s, %7$s, '%8$s');", data.getGencode(),data.getStatutTerminal(),data.getPrixCessionDistributeur() ,data.getColonneTurboweb(),data.getTypeInformation(),data.getDateDebut(),data.getValeurCalcul(),data.getDepartement());
			 statement.executeUpdate(query);
		 }
		
	}

}
