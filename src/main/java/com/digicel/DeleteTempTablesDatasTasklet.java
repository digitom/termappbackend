package com.digicel;

import javax.sql.DataSource;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.jdbc.core.JdbcTemplate;

public class DeleteTempTablesDatasTasklet implements Tasklet {

	private DataSource datasource;
	
	private static final String sql_ajustement_fid_temp = "delete from ajustement_fid_temp;";
	private static final String sql_caracteristiques_techniques_temp = "delete from caracteristiques_techniques_temp;";
	private static final String sql_mapping_msisdn_code_offre_temp = "delete from mapping_msisdn_code_offre_temp;";
	private static final String sql_mapping_offre_segment_temp = "delete from mapping_offre_segment_temp;";
	private static final String sql_mapping_reference_gencode_temp = "delete from mapping_reference_gencode_temp;";
	private static final String sql_informations_turboweb_temp = "delete from informations_turboweb_temp;";
	private static final String sql_statut_terminal_temp = "delete from statut_terminal_temp;";
	private static final String sql_delete_statut_termminal_temp = "delete from statut_terminal_temp;";
	private static final String sql_delete_turboweb_temp = "delete from turboweb_temp;";
	
	public  DataSource getDatasource() {
		return datasource;
	}

	public void setDatasource(DataSource datasource) {
		this.datasource = datasource;
	}
	
	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {

		JdbcTemplate template = new JdbcTemplate(getDatasource());
		
		template.execute(sql_ajustement_fid_temp);
		template.execute(sql_delete_turboweb_temp);
		template.execute(sql_delete_statut_termminal_temp);	
		template.execute(sql_mapping_reference_gencode_temp);
		template.execute(sql_caracteristiques_techniques_temp);
		template.execute(sql_mapping_offre_segment_temp);
		template.execute(sql_mapping_msisdn_code_offre_temp);
		template.execute(sql_statut_terminal_temp);
		template.execute(sql_informations_turboweb_temp);

		return RepeatStatus.FINISHED;
	}
}
