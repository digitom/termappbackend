package com.digicel;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import com.digicel.services.SendMailService;

@Component
public class BatchLauncher {

	private static SendMailService mailService;
	
	private static DataStructure GenerateAjustementFidJobParameters(File folder)
	{
		DataStructure result = null;
		
		File[] listOfFiles = folder.listFiles();		
		
		for (File file : listOfFiles) {
		    if (file.isFile()) {
		        if(file.getName().contains("ajustementFid"))
		        {
		        	result = new DataStructure();
		        	result.path = file.getPath();
		        }
		    }
		}
		return result;
	}
	
	private static DataStructure GenerateOffreColonneJobParameters(File folder)
	{
		DataStructure result = null;
		
		File[] listOfFiles = folder.listFiles();		
		
		for (File file : listOfFiles) {
		    if (file.isFile()) {
		        if(file.getName().contains("offreColonne"))
		        {
		        	result = new DataStructure();
		        	result.path = file.getPath(); 
		        	result.columnValue = file.getName().subSequence(0, 8).toString();  
		        }
		    }
		}
		return result;
	}
	
	private static DataStructure GenerateTurbowebInfosJobParameters(File folder,String departement)
	{
		DataStructure result = null;
		
		File[] listOfFiles = folder.listFiles();		
		
		for (File file : listOfFiles) {
		    if (file.isFile()) {
		        if(file.getName().contains(departement+"-turboweb-infos"))
		        {
		        	result = new DataStructure();
		        	result.path = file.getPath(); 
		        	result.columnValue = file.getName().subSequence(0, 8).toString();  
		        	file.getName().subSequence(9, 12).toString();
		        }
		    }
		}
		return result;
	}
	
	private static DataStructure GenerateTurbowebRenavJobParameters(File folder,String departement)
	{
		DataStructure result = null;
		
		File[] listOfFiles = folder.listFiles();		
		
		for (File file : listOfFiles) {
		    if (file.isFile()) {
		        if(file.getName().contains(departement+"-turboweb-renAV"))
		        {
		        	result = new DataStructure();
		        	result.path = file.getPath(); 
		        	result.columnValue = file.getName().subSequence(0, 8).toString();  
		        	file.getName().subSequence(9, 12).toString();
		        }
		    }
		}
		return result;
	}
	
	private static DataStructure GenerateTurbowebContporJobParameters(File folder,String departement)
	{
		DataStructure result = null;
		
		File[] listOfFiles = folder.listFiles();		
		
		for (File file : listOfFiles) {
		    if (file.isFile()) {
		        if(file.getName().contains(departement+"-turboweb-contPOR"))
		        {
		        	result = new DataStructure();
		        	result.path = file.getPath(); 
		        	result.columnValue = file.getName().subSequence(0, 8).toString();  
		        	file.getName().subSequence(9, 12).toString();
		        }
		    }
		}
		return result;
	}
	
	private static DataStructure GenerateTurbowebContavJobParameters(File folder,String departement)
	{
		DataStructure result = null;
		
		File[] listOfFiles = folder.listFiles();		
		
		for (File file : listOfFiles) {
		    if (file.isFile()) {
		        if(file.getName().contains(departement+"-turboweb-contAV"))
		        {
		        	result = new DataStructure();
		        	result.path = file.getPath(); 
		        	result.columnValue = file.getName().subSequence(0, 8).toString(); 
		        	file.getName().subSequence(9, 12).toString();
		        }
		    }
		}
		return result;
	}
	
	private static DataStructure GenerateReferenceGencodeJobParameters(File folder)
	{
		DataStructure result = null;
		
		File[] listOfFiles = folder.listFiles();		
		
		for (File file : listOfFiles) {
		    if (file.isFile()) {
		        if(file.getName().contains("gencodModele"))
		        {
		        	result = new DataStructure();
		        	result.path = file.getPath(); 
		        	result.columnValue = file.getName().subSequence(0, 8).toString();  
		        }
		    }
		}
		return result;
	}
	
	private static DataStructure GenerateMsisdnOffreFileJobParameters(File folder)
	{
		DataStructure result = null;
		
		File[] listOfFiles = folder.listFiles();		
		
		for (File file : listOfFiles) {
		    if (file.isFile()) {
		        if(file.getName().contains("msisdnOffers"))
		        {
		        	result = new DataStructure();
		        	result.path = file.getPath(); 
		        	result.columnValue = file.getName().subSequence(0, 8).toString();  
		        }
		    }
		}
		return result;
	}
	
	private static DataStructure GenerateCarateristiquesTechniquesJobParameters(File folder)
	{
		File[] listOfFiles = folder.listFiles();		
		DataStructure result = null;
		
		for (File file : listOfFiles) {
		    if (file.isFile()) {
		        if(file.getName().contains("Q"))
		        {
		        	result = new DataStructure();
		        	result.path = file.getPath(); 
		        	result.columnValue = file.getName().replace(".csv",""); 
		        }
		    }
		}
		return result;
	}
	
	
	private static Properties LoadProperties()
	{	
		Properties prop = new Properties();
		InputStream input = null;
		
		try {
			
			input = App.class.getResourceAsStream("/spring/batch/config/config.properties");
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return prop;
	}
	
	public void run() {
		
		String[] springConfig  = {"spring/batch/jobs/job-termapp.xml"};
		//new ClassPathXmlApplicationContext(springConfig);
		ApplicationContext context = new ClassPathXmlApplicationContext(springConfig);
				
		Properties properties = LoadProperties();
		
		File folderAjustementFid = new File(properties.getProperty("folderAjustementFid"));
		File folderCaracteristiquesTechniques = new File(properties.getProperty("folderCaracteristiquesTechniques"));
		File foldermsisdnOffre = new File(properties.getProperty("foldermsisdnOffre"));
		File folderReferenceGencode= new File(properties.getProperty("folderReferenceGencode"));
		File folderOffreColonne = new File(properties.getProperty("folderOffreColonne"));
		
		File folderTurbowebContav = new File(properties.getProperty("folderTurbowebContav"));
		File folderTurbowebContpor = new File(properties.getProperty("folderTurbowebContpor"));
		File folderTurbowebRenav = new File(properties.getProperty("folderTurbowebRenav"));
		File folderTurbowebInfos = new File(properties.getProperty("folderTurbowebInfos"));
			
		DataStructure ajustementFidJobParameters = GenerateAjustementFidJobParameters(folderAjustementFid);
		
		DataStructure msisdnOffreFileJobParameters = GenerateMsisdnOffreFileJobParameters(foldermsisdnOffre);
		
		DataStructure caracteristiquesTechniquesJobParameters = GenerateCarateristiquesTechniquesJobParameters(folderCaracteristiquesTechniques);
		DataStructure referenceGencodeJobParameters = GenerateReferenceGencodeJobParameters(folderReferenceGencode);
		DataStructure offreColonneJobParameters =  GenerateOffreColonneJobParameters(folderOffreColonne);
		
		DataStructure turbowebContavAnt =  GenerateTurbowebContavJobParameters(folderTurbowebContav,"ANT");
		DataStructure turbowebContavGuy =  GenerateTurbowebContavJobParameters(folderTurbowebContav,"GUY");
		DataStructure turbowebContavIdn =  GenerateTurbowebContavJobParameters(folderTurbowebContav,"IDN");
		
		DataStructure turbowebContporAnt = GenerateTurbowebContporJobParameters(folderTurbowebContpor,"ANT");
		DataStructure turbowebContporGuy = GenerateTurbowebContporJobParameters(folderTurbowebContpor,"GUY");
		DataStructure turbowebContporIdn = GenerateTurbowebContporJobParameters(folderTurbowebContpor,"IDN");
		
		DataStructure turbowebRenavAnt = GenerateTurbowebRenavJobParameters(folderTurbowebRenav,"ANT");
		DataStructure turbowebRenavGuy = GenerateTurbowebRenavJobParameters(folderTurbowebRenav,"GUY");
		DataStructure turbowebRenavIdn = GenerateTurbowebRenavJobParameters(folderTurbowebRenav,"IDN");
		
		DataStructure turbowebInfosAnt = GenerateTurbowebInfosJobParameters(folderTurbowebInfos,"ANT");
		DataStructure turbowebInfosGuy = GenerateTurbowebInfosJobParameters(folderTurbowebInfos,"GUY");
		DataStructure turbowebInfosIdn = GenerateTurbowebInfosJobParameters(folderTurbowebInfos,"IDN");
		
		//TODO : bouger ce code
		List<DataStructure> dataStructures = new ArrayList<DataStructure>();  
		
		dataStructures.add(ajustementFidJobParameters);
		dataStructures.add(msisdnOffreFileJobParameters);
		dataStructures.add(caracteristiquesTechniquesJobParameters);
		dataStructures.add(referenceGencodeJobParameters);
		dataStructures.add(offreColonneJobParameters);
		
		dataStructures.add(turbowebContavAnt);
		dataStructures.add(turbowebContavGuy);
		dataStructures.add(turbowebContavIdn);
		
		dataStructures.add(turbowebContporAnt);
		dataStructures.add(turbowebContporGuy);
		dataStructures.add(turbowebContporIdn);
		
		dataStructures.add(turbowebRenavAnt);
		dataStructures.add(turbowebRenavGuy);
		dataStructures.add(turbowebRenavIdn);
		
		dataStructures.add(turbowebInfosAnt);
		dataStructures.add(turbowebInfosGuy);
		dataStructures.add(turbowebInfosIdn);
		// FIN
		
		if(!Helpers.CheckDataStructurePath(dataStructures))
		{
			mailService = new SendMailService();
			JavaMailSenderImpl mailSender =  (JavaMailSenderImpl)context.getBean("mailSender");
			mailService.setFields(mailSender, properties.getProperty("mailSender"), properties.getProperty("mailDest") ,properties.getProperty("mailSubject") , "");
			mailService.sendMail();
			return;
		}
		
		JobParametersBuilder jpBuilder = new  JobParametersBuilder();
		
		jpBuilder.addString("input.caracteristiques.techniques.file.name", caracteristiquesTechniquesJobParameters.path);
		jpBuilder.addString("trimestre", caracteristiquesTechniquesJobParameters.columnValue);
		
		jpBuilder.addString("input.msisdn.offre.file.name", msisdnOffreFileJobParameters.path);
		jpBuilder.addString("dateMsisdnOffre", msisdnOffreFileJobParameters.columnValue);
		
		jpBuilder.addString("input.reference.gencode.file.name", referenceGencodeJobParameters.path);
		jpBuilder.addString("dateReferenceGencode", referenceGencodeJobParameters.columnValue);
		
		jpBuilder.addString("input.offre.colonne.file.name", offreColonneJobParameters.path);
		jpBuilder.addString("dateOffreColonne", offreColonneJobParameters.columnValue);
		
		
		jpBuilder.addString("input.contav.ant.file.name", turbowebContavAnt.path);
		jpBuilder.addString("dateTurbowebContavAnt", turbowebContavAnt.columnValue);

		jpBuilder.addString("input.contav.guy.file.name", turbowebContavGuy.path);
		jpBuilder.addString("dateTurbowebContavGuy", turbowebContavGuy.columnValue);
		
		jpBuilder.addString("input.contav.idn.file.name", turbowebContavIdn.path);
		jpBuilder.addString("dateTurbowebContavIdn", turbowebContavIdn.columnValue);
		
		
		jpBuilder.addString("input.contpor.ant.file.name", turbowebContporAnt.path);
		jpBuilder.addString("dateTurbowebContporAnt", turbowebContporAnt.columnValue);
		
		jpBuilder.addString("input.contpor.guy.file.name", turbowebContporGuy.path);
		jpBuilder.addString("dateTurbowebContporGuy", turbowebContporGuy.columnValue);
		
		jpBuilder.addString("input.contpor.idn.file.name", turbowebContporIdn.path);
		jpBuilder.addString("dateTurbowebContporIdn", turbowebContporIdn.columnValue);
		
		jpBuilder.addString("input.renav.ant.file.name", turbowebRenavAnt.path);
		jpBuilder.addString("dateTurbowebRenavAnt", turbowebRenavAnt.columnValue);	

		jpBuilder.addString("input.renav.guy.file.name", turbowebRenavGuy.path);
		jpBuilder.addString("dateTurbowebRenavGuy", turbowebRenavGuy.columnValue);	
		
		jpBuilder.addString("input.renav.idn.file.name", turbowebRenavIdn.path);
		jpBuilder.addString("dateTurbowebRenavIdn", turbowebRenavIdn.columnValue);	
		
		jpBuilder.addString("input.infos.ant.file.name", turbowebInfosAnt.path);
		jpBuilder.addString("dateTurbowebInfosAnt", turbowebInfosAnt.columnValue);	

		jpBuilder.addString("input.infos.guy.file.name", turbowebInfosGuy.path);
		jpBuilder.addString("dateTurbowebInfosGuy", turbowebInfosGuy.columnValue);	
		
		jpBuilder.addString("input.infos.idn.file.name", turbowebInfosIdn.path);
		jpBuilder.addString("dateTurbowebInfosIdn", turbowebInfosIdn.columnValue);	
		
		jpBuilder.addString("input.ajustementfid.file.name", ajustementFidJobParameters.path);
		
		JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
		Job job = (Job) context.getBean("termappJob");
		
		job.getJobParametersIncrementer().getNext(jpBuilder.toJobParameters());
		
		try {

			JobExecution execution = jobLauncher.run(job, jpBuilder.toJobParameters());
			System.out.println("Exit Status : " + execution.getStatus());
			/*for (DataStructure dataStructure : dataStructures) {
				Helpers.MoveFile(dataStructure.path);
			}*/
		
		} catch (Exception e) {
			e.printStackTrace();
		}	
		
		System.out.println("Done");	
	}
}