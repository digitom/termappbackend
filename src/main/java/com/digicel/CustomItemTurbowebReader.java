package com.digicel;

import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;

import com.digicel.model.Turboweb;

@Scope("step")
public class CustomItemTurbowebReader implements ItemReader<Turboweb> {
	
	private List<Turboweb> turbowebs = null;
	
	private String dateTurbowebAnt;
	private String dateTurbowebGuy;
	private String dateTurbowebIdn;
	
	private String filenameAnt;
	private String filenameGuy;
	private String filenameIdn;
	
	private int counter ;
	
	@Override
	public Turboweb read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		
		Turboweb turboweb = null;
		
		 if(counter >= turbowebs.size())
		 {
			 return null;
		 }
		 
		 try
		 {
			 turboweb = turbowebs.get(counter);
			 counter ++;
		 }
		 catch(Exception e) {
				e.printStackTrace();
		 }
		
		return  turboweb;
	}
	
	@Value("#{jobParameters['input.turboweb.ant.file.name']}")
	public void setFilenameAnt(final String filenameAnt)
	{
		this.filenameAnt = filenameAnt;
	}
	
	@Value("#{jobParameters['dateTurbowebAnt']}")
	public void setDateTurbowebAnt(final String dateTurbowebAnt)
	{
		this.dateTurbowebAnt = dateTurbowebAnt;
	}
	
	@Value("#{jobParameters['input.turboweb.guy.file.name']}")
	public void setFilenameGuy(final String filenameGuy)
	{
		this.filenameGuy = filenameGuy;
	}
	
	@Value("#{jobParameters['dateTurbowebGuy']}")
	public void setDateTurbowebGuy(final String dateTurbowebGuy)
	{
		this.dateTurbowebGuy = dateTurbowebGuy;
	}
	
	@Value("#{jobParameters['input.turboweb.idn.file.name']}")
	public void setFilenameIdn(final String filenameIdn)
	{
		this.filenameIdn = filenameIdn;
	}
	
	@Value("#{jobParameters['dateTurbowebIdn']}")
	public void setDateTurbowebIdn(final String dateTurbowebIdn)
	{
		this.dateTurbowebIdn = dateTurbowebIdn;
		
		turbowebs = new ArrayList<Turboweb>();
		turbowebs.add(new Turboweb(this.getDateTurbowebAnt(),this.getFilenameAnt(),"ANT"));
		turbowebs.add(new Turboweb(this.getDateTurbowebGuy(),this.getFilenameGuy(),"GUY"));
		turbowebs.add(new Turboweb(this.getDateTurbowebIdn(),this.getFilenameIdn(),"IDN"));
	}

	public String getDateTurbowebAnt() {
		return dateTurbowebAnt;
	}

	public String getDateTurbowebGuy() {
		return dateTurbowebGuy;
	}

	public String getDateTurbowebIdn() {
		return dateTurbowebIdn;
	}

	public String getFilenameAnt() {
		return filenameAnt;
	}

	public String getFilenameGuy() {
		return filenameGuy;
	}

	public String getFilenameIdn() {
		return filenameIdn;
	}
}
