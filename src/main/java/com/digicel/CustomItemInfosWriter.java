package com.digicel;

import java.sql.Connection;
import java.sql.Statement;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.batch.item.ItemWriter;
import org.springframework.context.annotation.Scope;

import com.digicel.model.DonneesAcquisition;
import com.digicel.model.MappingReferenceGencode;  
  
@Scope("step")
public class CustomItemInfosWriter implements ItemWriter<MappingReferenceGencode> {  
  
private Statement statement = null;

private DataSource datasource;

public  DataSource getDatasource() {
	return datasource;
}

public void setDatasource(DataSource datasource) {
	this.datasource = datasource;
}

@Override
public void write(List<? extends MappingReferenceGencode> datas) throws Exception {
	
	 Connection connection = datasource.getConnection();
		 
	 try 
	 {
		 for (MappingReferenceGencode data : datas)
		 {	
			 statement = connection.createStatement();
			 String queryAnt = String.format("insert into statut_terminal_temp(gencode,departement) VALUES ('%1$s', '%2$s');",data.getGencode(),"ANT");
			 String queryGuy= String.format("insert into statut_terminal_temp(gencode,departement) VALUES ('%1$s', '%2$s');",data.getGencode(),"GUY");
			 String queryIdn = String.format("insert into statut_terminal_temp(gencode,departement) VALUES ('%1$s', '%2$s');",data.getGencode(),"IDN");
			 				 
			 statement.executeUpdate(queryAnt);
			 statement.executeUpdate(queryGuy);
			 statement.executeUpdate(queryIdn);
		 }
	 }
	 catch (Exception e) 
	 {
		 throw new RuntimeException(e.getMessage());
	 }
	 
	}
}  
