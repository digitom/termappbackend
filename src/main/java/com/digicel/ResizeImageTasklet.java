
package com.digicel;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.activation.MimetypesFileTypeMap;
import javax.ejb.Stateless;
import javax.sql.DataSource;

//import org.jboss.seam.annotations.Name;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.apache.commons.io.FileUtils;

import com.digicel.services.ResizeImageService;
import com.thoughtworks.xstream.alias.ClassMapper.Null;

@Stateless
//@Name("ResizeImage")
public class ResizeImageTasklet implements Tasklet  {
	
	private DataSource datasource;
	
	public  DataSource getDatasource() {
		return datasource;
	}

	public void setDatasource(DataSource datasource) {
		this.datasource = datasource;
	}
	
	private ResizeImageService resizeImageService;
	private String imagesFolder = null;
	private String imagesTraiteesFolder = null;
	private String saveFolder = null;
	

	private static Properties LoadProperties()
	{	
		Properties prop = new Properties();
		InputStream input = null;
		
		try {
			
			input = App.class.getResourceAsStream("/spring/batch/config/config.properties");
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return prop;
	}
	
    private boolean FileIsAnImage(String file)
    {
    	 String mimetype = URLConnection.guessContentTypeFromName(file);
    	 if (mimetype == null)
    	 {
    		 return false;
    	 }
         String type = mimetype.split("/")[0];
         return type.equals("image");
    }
	
    private void GenerateNewImages() 
    {	
    	try
    	{
    		Properties properties = LoadProperties();
        	
    		imagesFolder = properties.getProperty("imagesFolder");
    		imagesTraiteesFolder = properties.getProperty("imagesTraiteesFolder");
    		saveFolder = properties.getProperty("saveFolder");
    		
    		//effacement des images temporaires
    		Helpers.DeleteFolder(saveFolder);
        	//sauvegarde des photos de production
    		FileUtils.copyDirectory(new File(imagesTraiteesFolder), new File(saveFolder), false);
    		
    		//effacement des images de production
    		Helpers.DeleteFolder(imagesTraiteesFolder);
    		
    		List<FolderDataStructure> brandsFolderList = Helpers.GetFolders(imagesFolder);
    		
    		for(FolderDataStructure brandFolder : brandsFolderList)
    		{	
    			//creation du dossier de la marque
    			Helpers.CreateFolder(imagesTraiteesFolder + brandFolder.folderName);
    			
    			List<FolderDataStructure> referenceFolders = Helpers.GetFolders(brandFolder.path);
    			
    			for(FolderDataStructure referenceFolder : referenceFolders)
    			{
    				List<FolderDataStructure> fileList = Helpers.GetFiles(referenceFolder.path);	
    				
    				//creation du dossier pour le modele
    				Helpers.CreateFolder(imagesTraiteesFolder + brandFolder.folderName +"/" +referenceFolder.folderName.subSequence(0, 9).toString());
    				
    				for(FolderDataStructure filePath : fileList)
    				{
    					File file = new File(filePath.path);
    					
    					if(FileIsAnImage(filePath.path))
    					{
    						resizeImageService.ResizeImage(file, imagesTraiteesFolder + brandFolder.folderName + "/" + referenceFolder.folderName.subSequence(0, 9).toString() + "/");
    					}
    				}
    			}
    		}
    	}
    	catch(Exception ex)
    	{
    		throw new RuntimeException(ex.getMessage());
    	}
		
    }
       
	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) {
		try
		{
			GenerateNewImages();
	    	//effacement des images temporaires
			Helpers.DeleteFolder(saveFolder);
		}
	    catch (Exception e) {
	    	try {
	    		//copie des fichiers sauvegardes vers la production
				FileUtils.copyDirectory(new File(saveFolder), new File(imagesTraiteesFolder), false);
		    	//effacement des images temporaires
				Helpers.DeleteFolder(saveFolder);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	    	throw new RuntimeException(e.getMessage());
		}

		return RepeatStatus.FINISHED;
	}

	public ResizeImageService getResizeImageService() {
		return resizeImageService;
	}

	public void setResizeImageService(ResizeImageService resizeImageService) {
		this.resizeImageService = resizeImageService;
	}

}
