package com.digicel;

public class FolderDataStructure
{	
	public FolderDataStructure(String path, String folderName) 
	{
		this.path = path;
		this.folderName = folderName;
	}
	
	public String path;
	public String folderName;
}

