package com.digicel;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import com.digicel.services.SendMailService;

public class App {
	
	
	
	private static Properties LoadProperties()
	{	
		Properties prop = new Properties();
		InputStream input = null;
		
		try {
			
			input = App.class.getResourceAsStream("/spring/batch/config/config.properties");
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return prop;
	}

	private static SendMailService mailService;
	
	public static void main(String[] args) 
	{	
		int argsLength = args.length;
		
		//if(argsLength != 0)
		//{
			String[] springConfig  = {"spring/batch/jobs/job-termapp.xml"};
			ApplicationContext context = new ClassPathXmlApplicationContext(springConfig);
			Properties properties = LoadProperties();
				
			File folderAjustementFid = new File(properties.getProperty("folderAjustementFid"));
			File folderCaracteristiquesTechniques = new File(properties.getProperty("folderCaracteristiquesTechniques"));
			File foldermsisdnOffre = new File(properties.getProperty("foldermsisdnOffre"));
			File folderReferenceGencode= new File(properties.getProperty("folderReferenceGencode"));
			File folderOffreColonne = new File(properties.getProperty("folderOffreColonne"));
			
			File folderTurbowebContav = new File(properties.getProperty("folderTurbowebContav"));
			File folderTurbowebContpor = new File(properties.getProperty("folderTurbowebContpor"));
			File folderTurbowebRenav = new File(properties.getProperty("folderTurbowebRenav"));
			File folderTurbowebInfos = new File(properties.getProperty("folderTurbowebInfos"));
			File folderTurboweb = new File(properties.getProperty("forlderTurbowebs"));
					
			DataStructure ajustementFidJobParameters = Helpers.GenerateAjustementFidJobParameters(folderAjustementFid);
			
			DataStructure msisdnOffreFileJobParameters = Helpers.GenerateMsisdnOffreFileJobParameters(foldermsisdnOffre);
			
			DataStructure caracteristiquesTechniquesJobParameters = Helpers.GenerateCarateristiquesTechniquesJobParameters(folderCaracteristiquesTechniques);
			DataStructure referenceGencodeJobParameters = Helpers.GenerateReferenceGencodeJobParameters(folderReferenceGencode);
			DataStructure offreColonneJobParameters =  Helpers.GenerateOffreColonneJobParameters(folderOffreColonne);
			
			DataStructure turbowebContavAnt =  Helpers.GenerateTurbowebContavJobParameters(folderTurbowebContav,"ANT");
			DataStructure turbowebContavGuy =  Helpers.GenerateTurbowebContavJobParameters(folderTurbowebContav,"GUY");
			DataStructure turbowebContavIdn =  Helpers.GenerateTurbowebContavJobParameters(folderTurbowebContav,"IDN");
			
			DataStructure turbowebContporAnt = Helpers.GenerateTurbowebContporJobParameters(folderTurbowebContpor,"ANT");
			DataStructure turbowebContporGuy = Helpers.GenerateTurbowebContporJobParameters(folderTurbowebContpor,"GUY");
			DataStructure turbowebContporIdn = Helpers.GenerateTurbowebContporJobParameters(folderTurbowebContpor,"IDN");
			
			DataStructure turbowebRenavAnt = Helpers.GenerateTurbowebRenavJobParameters(folderTurbowebRenav,"ANT");
			DataStructure turbowebRenavGuy = Helpers.GenerateTurbowebRenavJobParameters(folderTurbowebRenav,"GUY");
			DataStructure turbowebRenavIdn = Helpers.GenerateTurbowebRenavJobParameters(folderTurbowebRenav,"IDN");
			
			DataStructure turbowebInfosAnt = Helpers.GenerateTurbowebInfosJobParameters(folderTurbowebInfos,"ANT");
			DataStructure turbowebInfosGuy = Helpers.GenerateTurbowebInfosJobParameters(folderTurbowebInfos,"GUY");
			DataStructure turbowebInfosIdn = Helpers.GenerateTurbowebInfosJobParameters(folderTurbowebInfos,"IDN");
			
			DataStructure turbowebAnt =  Helpers.GenerateTurbowebJobParameters(folderTurboweb,"ANT");
			DataStructure turbowebGuy =  Helpers.GenerateTurbowebJobParameters(folderTurboweb,"GUY");
			DataStructure turbowebIdn =  Helpers.GenerateTurbowebJobParameters(folderTurboweb,"IDN");
			
			//TODO : bouger ce code
			List<DataStructure> dataStructures = new ArrayList<DataStructure>();  
			
			dataStructures.add(ajustementFidJobParameters);
			dataStructures.add(msisdnOffreFileJobParameters);
			dataStructures.add(caracteristiquesTechniquesJobParameters);
			dataStructures.add(referenceGencodeJobParameters);
			dataStructures.add(offreColonneJobParameters);
			
			dataStructures.add(turbowebContavAnt);
			dataStructures.add(turbowebContavGuy);
			dataStructures.add(turbowebContavIdn);
			
			dataStructures.add(turbowebContporAnt);
			dataStructures.add(turbowebContporGuy);
			dataStructures.add(turbowebContporIdn);
			
			dataStructures.add(turbowebRenavAnt);
			dataStructures.add(turbowebRenavGuy);
			dataStructures.add(turbowebRenavIdn);
			
			dataStructures.add(turbowebInfosAnt);
			dataStructures.add(turbowebInfosGuy);
			dataStructures.add(turbowebInfosIdn);
			
			dataStructures.add(turbowebAnt);
			dataStructures.add(turbowebGuy);
			dataStructures.add(turbowebIdn);
			
			// FIN
			
			if(!Helpers.CheckDataStructurePath(dataStructures))
			{
				mailService = new SendMailService();
				JavaMailSenderImpl mailSender =  (JavaMailSenderImpl)context.getBean("mailSender");
				mailService.setFields(mailSender, properties.getProperty("mailSender"), properties.getProperty("mailDest") ,properties.getProperty("mailSubject") , "");
				mailService.sendMail();
				return;
			}
			
			JobParametersBuilder jpBuilder = new  JobParametersBuilder();
			
			jpBuilder.addString("input.caracteristiques.techniques.file.name", caracteristiquesTechniquesJobParameters.path);
			jpBuilder.addString("trimestre", caracteristiquesTechniquesJobParameters.columnValue);
			
			jpBuilder.addString("input.msisdn.offre.file.name", msisdnOffreFileJobParameters.path);
			jpBuilder.addString("dateMsisdnOffre", msisdnOffreFileJobParameters.columnValue);
			
			jpBuilder.addString("input.reference.gencode.file.name", referenceGencodeJobParameters.path);
			jpBuilder.addString("dateReferenceGencode", referenceGencodeJobParameters.columnValue);
			
			jpBuilder.addString("input.offre.colonne.file.name", offreColonneJobParameters.path);
			jpBuilder.addString("dateOffreColonne", offreColonneJobParameters.columnValue);
			
			
			jpBuilder.addString("input.contav.ant.file.name", turbowebContavAnt.path);
			jpBuilder.addString("dateTurbowebContavAnt", turbowebContavAnt.columnValue);
			jpBuilder.addString("input.contav.guy.file.name", turbowebContavGuy.path);
			jpBuilder.addString("dateTurbowebContavGuy", turbowebContavGuy.columnValue);
			jpBuilder.addString("input.contav.idn.file.name", turbowebContavIdn.path);
			jpBuilder.addString("dateTurbowebContavIdn", turbowebContavIdn.columnValue);
			
			
			jpBuilder.addString("input.contpor.ant.file.name", turbowebContporAnt.path);
			jpBuilder.addString("dateTurbowebContporAnt", turbowebContporAnt.columnValue);
			jpBuilder.addString("input.contpor.guy.file.name", turbowebContporGuy.path);
			jpBuilder.addString("dateTurbowebContporGuy", turbowebContporGuy.columnValue);
			jpBuilder.addString("input.contpor.idn.file.name", turbowebContporIdn.path);
			jpBuilder.addString("dateTurbowebContporIdn", turbowebContporIdn.columnValue);
			
			
			jpBuilder.addString("input.renav.ant.file.name", turbowebRenavAnt.path);
			jpBuilder.addString("dateTurbowebRenavAnt", turbowebRenavAnt.columnValue);	
			jpBuilder.addString("input.renav.guy.file.name", turbowebRenavGuy.path);
			jpBuilder.addString("dateTurbowebRenavGuy", turbowebRenavGuy.columnValue);	
			jpBuilder.addString("input.renav.idn.file.name", turbowebRenavIdn.path);
			jpBuilder.addString("dateTurbowebRenavIdn", turbowebRenavIdn.columnValue);	
			
			jpBuilder.addString("input.infos.ant.file.name", turbowebInfosAnt.path);
			jpBuilder.addString("dateTurbowebInfosAnt", turbowebInfosAnt.columnValue);	
			jpBuilder.addString("input.infos.guy.file.name", turbowebInfosGuy.path);
			jpBuilder.addString("dateTurbowebInfosGuy", turbowebInfosGuy.columnValue);	
			jpBuilder.addString("input.infos.idn.file.name", turbowebInfosIdn.path);
			jpBuilder.addString("dateTurbowebInfosIdn", turbowebInfosIdn.columnValue);	
			
			jpBuilder.addString("input.turboweb.ant.file.name", turbowebAnt.path);
			jpBuilder.addString("dateTurbowebAnt", turbowebAnt.columnValue);	
			jpBuilder.addString("input.turboweb.guy.file.name", turbowebGuy.path);
			jpBuilder.addString("dateTurbowebGuy", turbowebGuy.columnValue);	
			jpBuilder.addString("input.turboweb.idn.file.name", turbowebIdn.path);
			jpBuilder.addString("dateTurbowebIdn", turbowebIdn.columnValue);	
			
			jpBuilder.addString("input.ajustementfid.file.name", ajustementFidJobParameters.path);
			
			JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
			Job job = (Job) context.getBean("termappJob");
			
			job.getJobParametersIncrementer().getNext(jpBuilder.toJobParameters());
			
			try {
	
				JobExecution execution = jobLauncher.run(job, jpBuilder.toJobParameters());
				
				System.out.println("Exit Status : " + execution.getStatus());
				
				/*for (DataStructure dataStructure : dataStructures) {
					Helpers.MoveFile(dataStructure.path);
				}*/
			
			} catch (Exception e) {
				e.printStackTrace();
			}	
			
			System.out.println("Done");
			
			
			/*else {
				String[] springConfig  = {"spring/batch/jobs/job-termapp.xml"};
				new ClassPathXmlApplicationContext(springConfig);
			}*/
		}
	}
