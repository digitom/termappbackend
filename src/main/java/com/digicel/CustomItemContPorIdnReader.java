package com.digicel;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;

import com.digicel.model.DonneesAcquisition;

@Scope("step")
public class CustomItemContPorIdnReader implements ItemReader<DonneesAcquisition>{
	
	private String fileName; 
	private int counter ;
	private String dateTurbowebContporIdn;

	List<DonneesAcquisition> myList = null;
	
	public CustomItemContPorIdnReader()
	{
		this.counter = 0;
	}

	@Override
	public DonneesAcquisition read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		 
		DonneesAcquisition valueToReturn = null;
		 
		 if(counter >= myList.size())
		 {
			 return null;
		 }
		 
		 try
		 {
			 valueToReturn = myList.get(counter);
			 counter ++;
		 }
		 catch(Exception e) {
				e.printStackTrace();
		 }	
		 return valueToReturn;
	}
	
	 @Value("#{jobParameters['dateTurbowebContporIdn']}")
	 public void setDateTurbowebContpor(final String date) 
	 {
		 this.setDateTurbowebContporIdn(date);
	 }
	 
	 @Value("#{jobParameters['input.contpor.idn.file.name']}")
	 public void setFileName(final String name) 
	 {
		 this.fileName = name;	 
	 
		 myList = new ArrayList<DonneesAcquisition>();
		 BufferedReader br = null;
			String line = "";
			String cvsSplitBy = ";";
			int iteration = 0;
			
			try {

				br = new BufferedReader(new FileReader(fileName));
				while ((line = br.readLine()) != null) {
						
					  if(iteration < 3) {
					        iteration++;  
					        continue;
					    }
					  
			        String[] lineValues = line.split(cvsSplitBy);
					
					for(int i = 7; i <= lineValues.length-1; i++ )
					{
						DonneesAcquisition value = new DonneesAcquisition(lineValues[0].replace(" ", ""), Integer.parseInt(lineValues[6].replace(" ", "")),lineValues[3],Integer.parseInt(lineValues[i].replace(" ", "")),i-6,"contpor",this.getDateTurbowebContporIdn(),"IDN");
						myList.add(value);
					}
				}

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		 	  catch(Exception e) {
					e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
	 	}

	public String getDateTurbowebContporIdn() {
		return dateTurbowebContporIdn;
	}

	public void setDateTurbowebContporIdn(String dateTurbowebContporIdn) {
		this.dateTurbowebContporIdn = dateTurbowebContporIdn;
	}	
}
