package com.digicel;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.mail.javamail.JavaMailSender;

import com.digicel.services.SendMailService;


public class StatusMailJobListener implements JobExecutionListener {
	    
    private SendMailService sendMailService;
    private JavaMailSender mailSender;
    private String senderAddress;
    private String recipientError;
    private String recipientSuccess;
    private String subjectError;
    private String subjectSuccess;
    private String bodySuccess;
 
    public void setMailSender(JavaMailSender mailSender) {
       this.mailSender = mailSender;
    }
    public void setSenderAddress(String senderAddress) {
        this.senderAddress = senderAddress;
    }
 
    public void setRecipientError(String recipientError) {
    	this.recipientError = recipientError;
    }

    public void setRecipientSuccess(String recipientSuccess) {
    	this.recipientSuccess = recipientSuccess;
    }
 
    public void setSendMailService(SendMailService sendMailService) {
        this.sendMailService = sendMailService;
    }    
    
    public void setSubjectError(String subjectError) {
    	this.subjectError = subjectError;
    }
    
    public void setSubjectSuccess(String subjectSuccess) {
    	this.subjectSuccess = subjectSuccess;
    }

    public void setBodySuccess(String bodySuccess) {
    	this.bodySuccess = bodySuccess;
    }
    
  @Override
  public void afterJob(JobExecution jobExecution) {
	  
        String exitCode = jobExecution.getExitStatus().getExitCode();
        
        try {
        
        	if (exitCode.equals(ExitStatus.COMPLETED.getExitCode())) 
        	{
                sendMailService.setFields(mailSender, senderAddress, recipientSuccess, subjectSuccess, bodySuccess);
        	} 
        	else 
        	{
                String innerException = jobExecution.getExecutionContext().getString("error");
                sendMailService.setFields(mailSender, senderAddress, recipientError, subjectError, innerException);
        	}
        	
            sendMailService.sendMail();
            
        } catch (Exception ex) {
        	
        	throw new RuntimeException(ex.getMessage());
        }
    }

	@Override
	public void beforeJob(JobExecution arg0) 
	{
		
	}

}



